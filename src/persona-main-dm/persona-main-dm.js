import { LitElement, html } from 'lit-element';

class PersonaMainDm extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
        };
    }

    constructor () {
        super();

        this.people = [
            {
                name: "Pepito Perez",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Pepito Perez"
                }
            },
            {
                name: "Bruce Willis",
                yearsInCompany: 2,
                profile: "El veloz murciélago hindú comía feliz cardillo y kiwi",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Willis"
                }
            },
            {
                name: "Peter",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Peter"
                }
            },
            {
                name: "Han Solo",
                yearsInCompany:9,
                profile: "El veloz murciélago hindú",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Han Solo"
                }
            },
            {
                name: "Bruce Lee",
                yearsInCompany: 6,
                profile: "El veloz",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Lee"
                }
            }
        ];

        this.showPersonForm = false;
    }
    updated (changedProperties) {
        console.log ("updated");
    
        if (changedProperties.has("people")) {
            console.log("Ha cambiado la propiedad people en persona-main-dm");

            this.dispatchEvent(new CustomEvent("people-data-updated", {
                detail: {
                    people: this.people
                }
            }))
        }
    }
}



customElements.define('persona-main-dm', PersonaMainDm)