import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: {type: Object},
            rangeValue: {type: String},
            maxYearsInCompanyFilter: {type: Number}
        };
    }

    constructor () {
        super();

        this.peopleStats = {};
 
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                        Hay <span class="badge bg-bill bg-primary">${this.peopleStats.numberOfPeople}</span> personas.
                    </div>
                    <div class="mt-5">
                        <button class="w-100 btn bg-success" style="font-size: 50px"
                        @click="${this.newPerson}"><strong>+</strong></button>
                    </div>
                    <div class="mt-5">
                        <input type="range" min"0" max="${this.peopleStats.maxYearsInCompany}"
                         step="1"
                          value="${this.peopleStats.maxYearsInCompany}"
                          @input="${this.updateMaxYearsInCompanyFilter}"
                           class="form-range" id="myRange">
                        <p> Años de antiguedad: <span id="yearsInCompany"> ${this.rangeValue}</span></p>
                    </div>
                </section>
            </aside>
        `;
    }

    newPerson(e) {
        console.log("newPerson es persona-sidebar");
        console.log("Se va a crear una persona");

        this.dispatchEvent(new CustomEvent("new-person", {}))
    }

    updateRangeValue (e) {
        this.rangeValue = e.srcElement.value
        console.log(e.srcElement.value)
        this.dispatchEvent(new CustomEvent("updated-range",{
            detail: {
                rangeValue: e.srcElement.value
            }
        }))
      }

    updateMaxYearsInCompanyFilter (e) {
        console.log("updateMaxYearsInCompanyFilter");
        console.log("updateMaxYearsInCompanyFilter");

        this.dispatchEvent(new CustomEvent("updated-max-years-filter",{
            detail: {
                maxYearsInCompanyFilter: e.target.value
            }
        }))

    }
}

customElements.define('persona-sidebar', PersonaSidebar)